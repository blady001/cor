package cor;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by macblady on 02.01.2018.
 */
public class Utils {
    public static class PrettyPrintingMap<K, V> {
        private Map<K, V> map;

        public PrettyPrintingMap(Map<K, V> map) {
            this.map = map;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder();
            Iterator<Map.Entry<K, V>> iter = map.entrySet().iterator();
            while (iter.hasNext()) {
                Map.Entry<K, V> entry = iter.next();
                sb.append(entry.getKey());
                sb.append('=').append('"');
                sb.append(entry.getValue());
                sb.append('"');
                if (iter.hasNext()) {
                    sb.append(',').append(' ');
                }
            }
            return sb.toString();

        }
    }

    public static JSONArray getUserData() {
        JSONObject user1 = new JSONObject();
        user1.put("id", 1);
        user1.put("username", "User 1");

        JSONObject user2 = new JSONObject();
        user2.put("id", 2);
        user2.put("username", "User 2");

        JSONArray list = new JSONArray();
        list.add(user1);
        list.add(user2);
        return list;
    }

    public static void printInputStream(InputStream is) {
        try {
            final BufferedReader reader = new BufferedReader(
                    new InputStreamReader(is));
            String line = null;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
            reader.close();
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    public static String convertInputStreamToString(InputStream is) {
        String result = "";
        try {
            final BufferedReader reader = new BufferedReader(
                    new InputStreamReader(is));
            String line = null;
            while ((line = reader.readLine()) != null) {
                result += line;
            }
            reader.close();
        } catch (final Exception e) {
            e.printStackTrace();
        } finally {
            return result;
        }
    }
}
