package cor;

/**
 * Created by macblady on 12.12.2017.
 */
public class HTMLTemplate implements BaseTemplate {
    private String htmlStr;

    public HTMLTemplate(String htmlStr) {
        this.htmlStr = htmlStr;
    }

    public String returnHTML() {
        return this.htmlStr;
    }
}
