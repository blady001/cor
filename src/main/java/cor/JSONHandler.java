package cor;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import db.DbHandler;
import db.Record;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.toIntExact;

/**
 * Created by macblady on 02.01.2018.
 */
public class JSONHandler extends AbstractHandler {

    private HttpHandler nextHandler = null;

    private DbHandler dbHandler;

    private static final String HEADER_ALLOW = "Allow";
    private static final String HEADER_CONTENT_TYPE = "Content-Type";
    private static final Charset CHARSET = StandardCharsets.UTF_8;
    private static final int STATUS_OK = 200;
    private static final String SQL_FAILURE = "{\"error\": \"couldn't connect to db\"}";

    public JSONHandler(DbHandler dbHandler) {
        this.dbHandler = dbHandler;
    }

    @Override
    public void setNext(HttpHandler handler) {
        this.nextHandler = handler;
    }

    @Override
    public void addEndpoint(String endpoint) {
        this.endpoints.add(endpoint);
    }

    private JSONObject convertRecordToJsonObj(Record record) {
        JSONObject obj = new JSONObject();
        for (String key : record.values.keySet()) {
            Object value = record.values.get(key);
            obj.put(key, value);
        }
        return obj;
    }

    private String deletePattern(HttpExchange httpExchange) throws SQLException {
        String id = httpExchange.getRequestURI().getPath().split("/")[3];
        String sql = "delete from tab_dp where id=" + id + ";";
        this.dbHandler.delete(sql);
        return this.buildPatternsList();
    }

    private String deleteType(HttpExchange httpExchange) throws SQLException {
        String id = httpExchange.getRequestURI().getPath().split("/")[3];
        String sql = "delete from tab_type where id=" + id + ";";
        this.dbHandler.delete(sql);
        return "{\"status\":\"Ok, type deleted. Go to /types/\"}";
    }

    private String insertNewPattern(HttpExchange httpExchange) throws ParseException, SQLException {
        String data = Utils.convertInputStreamToString(httpExchange.getRequestBody());
        JSONParser parser = new JSONParser();
        JSONObject newPattern = (JSONObject) parser.parse(data);
        String name = (String) newPattern.get("name");
        String description = (String) newPattern.get("description");
        String sql = "insert into tab_dp (name, description) values ('" + name + "', '" + description + "');";
        System.out.println(sql);
        this.dbHandler.insert(sql);
        return this.buildPatternsList();
    }

    private String insertNewType(HttpExchange httpExchange) throws SQLException, ParseException {
        String data = Utils.convertInputStreamToString(httpExchange.getRequestBody());
        JSONParser parser = new JSONParser();
        JSONObject newType = (JSONObject) parser.parse(data);
        String name = (String) newType.get("name");
        String codename = (String) newType.get("codename");
        String sql = "insert into tab_type (name, codename) values ('" + name + "', '" + codename + "');";
//        System.out.println(sql);
        this.dbHandler.insert(sql);
        return "{\"status\":\"Ok, type inserted. Go to /types/\"}";
    }

    private String createNewInfo(HttpExchange httpExchange) throws SQLException, ParseException {
        String data = Utils.convertInputStreamToString(httpExchange.getRequestBody());
        JSONParser parser = new JSONParser();
        JSONObject newInfo = (JSONObject) parser.parse(data);
        String content = (String) newInfo.get("content");
        String sql = "update tab_info set content = '" + content + "';";
        System.out.println(sql);
        this.dbHandler.update(sql);
        return "{\"status\":\"Ok, info updated. Go to /main/\"}";
    }

    private String updatePattern(HttpExchange httpExchange) throws ParseException, SQLException {
        String id = httpExchange.getRequestURI().getPath().split("/")[3];
        String data = Utils.convertInputStreamToString(httpExchange.getRequestBody());
        JSONParser parser = new JSONParser();
        JSONObject patternData = (JSONObject) parser.parse(data);
        String name = (String) patternData.get("name");
        String description = (String) patternData.get("description");
        String sql = "update tab_dp set name = '" + name + "', description = '" + description + "' where id = " + id + ";";
        this.dbHandler.update(sql);
        return this.buildPatternsList();
    }

    private String buildPatternsList() {
        String content = "";
        try {
            List<Record> patterns = dbHandler.getAll("tab_dp");
            JSONArray arr = new JSONArray();
            for (Record r : patterns) {
                arr.add(this.convertRecordToJsonObj(r));
            }
            content = arr.toJSONString();
        } catch (SQLException e) {
            e.printStackTrace();
            content = "{\"message\":\"problem with data fetching\"}";
        }
        return content;
    }

    private String buildResponse(HttpExchange httpExchange) {
        String content = null;
        String path = httpExchange.getRequestURI().getPath();

        final String requestMethod = httpExchange.getRequestMethod().toUpperCase();
        final String[] pathSplitted = path.split("/");

        try {

            if (pathSplitted[2].equals("patterns")) {
                switch (requestMethod) {
                    case "GET":
                        content = this.buildPatternsList();
                        break;
                    case "POST":
                        content = this.insertNewPattern(httpExchange);
                        break;
                    default:
                        break;
                }
            } else if (pathSplitted[2].equals("pattern")) {
                switch (requestMethod) {
                    case "PUT":
                        content = this.updatePattern(httpExchange);
                        break;
                    case "DELETE":
                        content = this.deletePattern(httpExchange);
                        break;
                    default:
                        break;
                }
            } else if (pathSplitted[2].equals("types")) {
                content = this.insertNewType(httpExchange);
            } else if (pathSplitted[2].equals("type")) {
                content = this.deleteType(httpExchange);
            } else if (pathSplitted[2].equals("info")) {
                content = this.createNewInfo(httpExchange);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (content == null) {
            content = "{\"message\":\"unsupported method\"}";
        }

        return content;
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
//        System.out.println("[JSONHandler]: " + httpExchange.getRequestURI().getPath());
        if (this.canHandle(httpExchange)) {
            String response = this.buildResponse(httpExchange);
            Headers headers = httpExchange.getResponseHeaders();
            headers.set(HEADER_CONTENT_TYPE, String.format("application/json; charset=%s", CHARSET));

            httpExchange.sendResponseHeaders(200, response.length());
            OutputStream os = httpExchange.getResponseBody();
            os.write(response.getBytes());
            os.close();
        } else {
            this.nextHandler.handle(httpExchange);
        }

    }
}
