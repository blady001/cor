package cor;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by macblady on 22.01.2018.
 */
public abstract class AbstractHandler implements HttpHandler, Chainable {
    protected List<String> endpoints = new ArrayList<>();

    @Override
    public boolean canHandle(HttpExchange httpExchange) {
        String path = httpExchange.getRequestURI().getPath();
        String[] splitted = path.split("/");
        if (splitted[splitted.length - 1].matches("\\d+")) {
            String newPath = "";
            splitted[splitted.length - 1] = "<int>";
            for (int i = 1; i < splitted.length; i++) {
                newPath += "/" + splitted[i];
            }
            path = newPath;
        }
        return this.endpoints.contains(path);
    }

}
