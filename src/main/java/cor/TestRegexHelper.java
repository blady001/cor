package cor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by macblady on 12.12.2017.
 */
public class TestRegexHelper {
    public static void main(String[] args) {
        String template = "<html><head>{{var1}}</head><body>{{var_2}}</body></html>";
        RegexHelper helper = new RegexHelper(template);

        List<String> variables = helper.getAllMatches();
        System.out.println("Matches found: ");
        for (String match: variables) {
            System.out.println(match);
        }

        System.out.println("\nReplaced content:");
        Map<String, String> replacement = new HashMap<String, String>();
        replacement.put("var1", "test1");
        replacement.put("var_2", "test_2");
        String newHtml = helper.getReplacedContent(replacement);
        System.out.println(newHtml);
    }
}
