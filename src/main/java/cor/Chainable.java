package cor;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

/**
 * Created by macblady on 08.01.2018.
 */
public interface Chainable {
    void setNext(HttpHandler handler);
    void addEndpoint(String endpoint);
    boolean canHandle(HttpExchange httpExchange);
}
