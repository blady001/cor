package cor;

import java.io.IOException;
import java.util.List;

/**
 * Created by macblady on 02.01.2018.
 */
public class CSVReaderTest {

    public static void main(String[] args) {
        CSVReader reader = new CSVReader("routing.csv", ",");
        try {
            List<List<String>> data = reader.read();
            System.out.println("Length: " + Integer.toString(data.size()));
            int i = 1;
            for (List<String> record : data) {
                for (String word : record) {
                    System.out.println("Record #" + i + ": " + word);
                }
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
