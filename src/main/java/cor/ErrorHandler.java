package cor;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by macblady on 02.01.2018.
 */
public class ErrorHandler implements HttpHandler, Chainable {
    @Override
    public void setNext(HttpHandler handler) {

    }

    @Override
    public void addEndpoint(String endpoint) {

    }

    @Override
    public boolean canHandle(HttpExchange httpExchange) {
        return true;
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        String response = "[ErrorHandler]: No routing defined for: " + httpExchange.getRequestURI().getPath();
        System.out.println(response);
        httpExchange.sendResponseHeaders(200, response.length());
        OutputStream os = httpExchange.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }
}
