package cor;

import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.Map;

/**
 * Created by macblady on 12.12.2017.
 */
public class BuilderTest {
    public static void main(String[] args) throws IOException, ParseException {
        TemplateBuilder builder = new HTMLTemplateBuilder();

        builder.setHtmlTemplatePath("sample.html");
        CustomJsonParser parser = CustomJsonParser.parse("templateConf.json");
        Map<String, String> replacement = parser.createVariableMapping();

        builder.setContent(replacement);
        BaseTemplate template = builder.build();
        System.out.println(template.returnHTML());
    }
}
