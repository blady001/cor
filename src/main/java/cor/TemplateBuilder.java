package cor;

import java.util.Map;

/**
 * Created by macblady on 12.12.2017.
 */
public interface TemplateBuilder {
    BaseTemplate build();
    void setContent(Map<String, String> content);
    void setHtmlTemplatePath(String path);
}
