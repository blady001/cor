package cor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by macblady on 12.12.2017.
 */
public class RegexHelper {
    private String pattern = "\\{\\{([^}]*)\\}\\}";
    private String content;

    public RegexHelper(String content){
        this.content = content;
    }

    public RegexHelper(String content, String pattern) {
        this.content = content;
        this.pattern = pattern;
    }

    public List<String> getAllMatches() {
        List<String> allMatches = new ArrayList<String>();
        Pattern p = Pattern.compile(this.pattern);
        Matcher m = p.matcher(this.content);
        while (m.find()) {
            allMatches.add(m.group(1));
        }
        return allMatches;
    }

    public String getReplacedContent(Map<String, String> mapping) {
        String newContent = new String(this.content);
        for (Map.Entry<String, String> entry : mapping.entrySet()) {
            String key = "\\{\\{" + entry.getKey() + "\\}\\}";
            String value = entry.getValue();
            newContent = newContent.replaceAll(key, value);
        }
        return newContent;
    }
}
