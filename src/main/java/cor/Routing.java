package cor;

import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import db.DBConfig;
import db.DbHandler;

import java.net.InetSocketAddress;
import java.util.List;

/**
 * Created by macblady on 02.01.2018.
 */
public class Routing {
    private String csvRoutingPath = "routing.csv";
    private String csvSeparator = ",";

    private HttpHandler configureChain() throws Exception {
        DBConfig dbConfig = DBConfig.getConfig("data/dbConf.json");
        DbHandler dbHandler = new DbHandler(dbConfig);

        MainHandler mainHandler = new MainHandler(dbHandler);
        JSONHandler jsonHandler = new JSONHandler(dbHandler);
        ErrorHandler errorHandler = new ErrorHandler();

        mainHandler.setNext(jsonHandler);
        jsonHandler.setNext(errorHandler);

        CSVReader reader = new CSVReader(this.csvRoutingPath, this.csvSeparator);
        List<List<String>> csvData = reader.read();

        for (List<String> mapping : csvData) {
            String endpoint = mapping.get(0);
            String handler = mapping.get(1);

            switch (handler) {
                case "MainHandler":
                    mainHandler.addEndpoint(endpoint);
                    break;
                case "JSONHandler":
                    jsonHandler.addEndpoint(endpoint);
                    break;
                default:
                    errorHandler.addEndpoint(endpoint);
            }
        }
        return mainHandler;
    }

    private void start() throws Exception {
        HttpServer server = HttpServer.create(new InetSocketAddress(9000), 0);

        HttpHandler firstHandler = this.configureChain();
        server.createContext("/", firstHandler);
        server.setExecutor(null); // creates a default executor
        server.start();
    }

    public static void main(String[] args) throws Exception {
        Routing server = new Routing();
        server.start();
        System.out.println("Server started!");
    }

}
