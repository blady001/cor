package cor;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import db.DbHandler;
import db.Record;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by macblady on 02.01.2018.
 */
public class MainHandler extends AbstractHandler {

    private static final String MAPPING_FILENAME = "mainHandlerMappingConf.json";
    private HttpHandler nextHandler = null;
//    private List<String> endpoints = new ArrayList<>();
    private DbHandler dbHandler;

    public MainHandler(DbHandler dbHandler) {
        this.dbHandler = dbHandler;
    }


    private String resolveDataFilePath(String uri) {
        try {
            CustomJsonParser parser = CustomJsonParser.parse(MAPPING_FILENAME);
            return parser.getString(uri);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    @Override
    public void addEndpoint(String endpoint) {
        this.endpoints.add(endpoint);
    }

    @Override
    public void setNext(HttpHandler obj) {
        this.nextHandler = obj;
    }

    private Map<String, String> getPatternContent(String id) {
        Map<String, String> data = new HashMap<>();
        try {
            Record r = dbHandler.getOne("tab_dp", "id=" + id);
            data.put("patternName", (String) r.values.get("name"));
            data.put("patternDescription", (String) r.values.get("description"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return data;
    }

    private String getInfo() {
        String returnVal = null;
        try {
            List<Record> lr = dbHandler.getAll("tab_info");
            Record r = lr.get(0);
            returnVal = (String) r.values.get("content");
        } catch (SQLException e) {
            e.printStackTrace();
            returnVal = "ERROR while retreving info";
        }
        return returnVal;
    }

    private String getPatternsListContent() {
        String result = "";
        try {
            List<Record> patterns = dbHandler.getAll("tab_dp");
            StringBuilder bld = new StringBuilder();
            for (Record r : patterns) {
                bld.append("<li><a href=\"/pattern/");
                bld.append(r.values.get("id"));
                bld.append("\">");
                bld.append(r.values.get("name"));
                bld.append("</a></li>");
            }
            result = bld.toString();
        } catch (SQLException e) {
            e.printStackTrace();
            result = "ERROR in getPatternsListContent()";
        }
        return result;
    }

    private String getTypesListContent() {
        String result = "";
        try {
            List<Record> patterns = dbHandler.getAll("tab_type");
            StringBuilder bld = new StringBuilder();
            for (Record r : patterns) {
                bld.append("<li>");
//                bld.append(r.values.get("id"));
//                bld.append("\">");
                bld.append(r.values.get("name"));
                bld.append("</li>");
            }
            result = bld.toString();
        } catch (SQLException e) {
            e.printStackTrace();
            result = "ERROR in getPatternsListContent()";
        }
        return result;
    }

    private String buildPatternPage(String id) {
        TemplateBuilder builder = new HTMLTemplateBuilder();
        builder.setHtmlTemplatePath("templates/pattern_page.html");
        Map<String, String> content = this.getPatternContent(id);
        content.put("pageTitle", "Wszystkie wzorce");
        builder.setContent(content);
        return builder.build().returnHTML();
    }

    private String buildPatternsListPage() {
        TemplateBuilder builder = new HTMLTemplateBuilder();
        builder.setHtmlTemplatePath("templates/patterns_list.html");
        Map<String, String> content = new HashMap<>();
        content.put("pageTitle", "Wszystkie wzorce");
        content.put("pageContent", this.getPatternsListContent());
        builder.setContent(content);
        return builder.build().returnHTML();
    }

    private String buildTypesListPage() {
        TemplateBuilder builder = new HTMLTemplateBuilder();
        builder.setHtmlTemplatePath("templates/patterns_list.html");
        Map<String, String> content = new HashMap<>();
        content.put("pageTitle", "Wszystkie wzorce");
        content.put("pageContent", this.getTypesListContent());
        builder.setContent(content);
        return builder.build().returnHTML();
    }

    private String buildMainPage() {
        TemplateBuilder builder = new HTMLTemplateBuilder();
        builder.setHtmlTemplatePath("templates/main_page.html");
        Map<String, String> content = new HashMap<>();
        content.put("pageDescription", this.getInfo());
        content.put("pageTitle", "Wzorce projektowe");
        builder.setContent(content);
        return builder.build().returnHTML();
    }

    private String buildResponse(HttpExchange httpExchange) {
        String content;
        String path = httpExchange.getRequestURI().getPath();

        switch (path.split("/")[1]) {
            case "main":
                content = this.buildMainPage();
                break;
            case "patterns":
                content = this.buildPatternsListPage();
                break;
            case "pattern":
                content = this.buildPatternPage(path.split("/")[2]);
                break;
            case "types":
                content = this.buildTypesListPage();
                break;
            default:
                content = "not yet xd";
        }
        return content;

    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
//        System.out.println("[MainHandler]: " + httpExchange.getRequestURI().getPath());

        if (this.canHandle(httpExchange)) {
            String response = this.buildResponse(httpExchange);
            httpExchange.sendResponseHeaders(200, response.length());
            OutputStream os = httpExchange.getResponseBody();
            os.write(response.getBytes());
            os.close();
        } else {
            this.nextHandler.handle(httpExchange);
        }
    }
}
