package cor;

import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by macblady on 12.12.2017.
 */
public class HTMLTemplateBuilder implements TemplateBuilder {
    private String htmlTemplatePath;
    private Map<String, String> content;

    public HTMLTemplateBuilder() {

    }

    public void setHtmlTemplatePath(String path) {
        this.htmlTemplatePath = path;
    }

    @Override
    public void setContent(Map<String, String> content) {
        this.content = content;
    }

    private String readTemplate() throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(this.htmlTemplatePath));
        return new String(encoded, StandardCharsets.UTF_8);
    }

    private Map<String, String> createVariableMapping(List<String> variableNames, CustomJsonParser parser) {
        Map<String, String> mapping = new HashMap<>();
        for (String var : variableNames) {
            String value = parser.getString(var);
            mapping.put(var, value);
        }
        return mapping;
    }

    @Override
    public BaseTemplate build() {
        String htmlStr = "";
        try {
            String templateStr = this.readTemplate();
            HTMLTemplate template = new HTMLTemplate(templateStr);
            JSONDataTemplateDecorator decorated = new JSONDataTemplateDecorator(template, this.content);
            return decorated;
        } catch (IOException e) {
            e.printStackTrace();
            return new HTMLTemplate("<p>Error in HTMLTemplateBuilder's build()!</p>");
        }
//        try {
//            String templateStr = this.readTemplate();
//            RegexHelper reHelper = new RegexHelper(templateStr);
//            List<String> variables = reHelper.getAllMatches();
//            htmlStr = reHelper.getReplacedContent(this.content);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return new HTMLTemplate(htmlStr);
    }
}
