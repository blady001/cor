package cor;

import java.util.Map;

/**
 * Created by macblady on 23.01.2018.
 */
public class JSONDataTemplateDecorator implements BaseTemplate {
    private BaseTemplate template;
    private Map<String, String> content;
    private String value;

    public JSONDataTemplateDecorator(BaseTemplate template, Map<String, String> content) {
        this.template = template;
        this.content = content;

        RegexHelper regexHelper = new RegexHelper(this.template.returnHTML());
        this.value = regexHelper.getReplacedContent(content);
    }

    public String returnHTML() {
        return this.value;
    }
}
