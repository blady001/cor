package db;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;

/**
 * Created by macblady on 29.11.2017.
 */
public class DBConfig {

//    private final String CONFIG = "dbConf.json";

    private String url;
    private Long port;
    private String username;
    private String password;
    private String dbName;
    private String dbType;

    private String confFileName;

    private DBConfig(String confFileName) throws ParseException {
        this.confFileName = confFileName;
        this.readConfig();
    }

    public static DBConfig getConfig(String confFileName) throws ParseException {
        return new DBConfig(confFileName);
    }

    public String getUrl() {
        return url;
    }

    public Long getPort() {
        return port;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getDbName() {
        return dbName;
    }

    public String getDbType() {
        return dbType;
    }

//    public void writeConfig() throws IOException {
//        JSONObject config = new JSONObject();
//        config.put("url", "sqlite://...");
//        config.put("port", 3166);
//        config.put("username", "admin");
//        config.put("password", "admin");
//        config.put("database", "mbase");
//
//        FileWriter configFile = null;
//        try {
//            configFile = new FileWriter(CONFIG);
//            configFile.write(config.toJSONString());
//        } catch(IOException e) {
//            e.printStackTrace();
//        } finally {
//            configFile.close();
//        }
//
//    }

    public void readConfig() throws ParseException {
        JSONParser parser = new JSONParser();
        FileReader configFile = null;
        String jsonConfig = "";

        try {
            configFile = new FileReader(this.confFileName);
            JSONObject obj = (JSONObject) parser.parse(configFile);
            this.readConfigFromJson(obj);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                configFile.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void readConfigFromJson(JSONObject o) {
        this.url = (String) o.get("url");
        this.port = (Long) o.get("port");
        this.username = (String) o.get("username");
        this.password = (String) o.get("password");
        this.dbName = (String) o.get("dbName");
        this.dbType = (String) o.get("dbType");
    }
}
