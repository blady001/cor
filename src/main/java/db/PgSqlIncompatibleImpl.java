package db;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by macblady on 20.11.2017.
 */
public class PgSqlIncompatibleImpl implements PgSqlIncompatible {
    public Record getOneFromPg(String tableName, String whereClause) throws SQLException {
        return null;
    }

    public List<Record> getAllFromPg(String tableName) throws SQLException {
        return null;
    }

    public void insertToPg(String sql) throws SQLException {

    }

    public void deleteFromPg(String sql) throws SQLException {

    }

    public void updateInPg(String sql) throws SQLException {

    }
}
