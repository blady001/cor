package db;

import org.json.simple.parser.ParseException;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by macblady on 05.12.2017.
 */
public class DbHandlerTest {
    public static void main(String[] args) {

        try {
            DBConfig config = DBConfig.getConfig("data/dbConf.json");

            DbHandler handler = new DbHandler(config);

            List<Record> students = handler.getAll("tab_student");

            for (Record r : students) {
                System.out.println("Student: " + (String) r.values.get("name"));
            }

        } catch (ParseException e) {
            e.printStackTrace();
        } catch (DbResolver.DbConnectorResolvingException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
//        SqliteDbManager handler2 = SqliteDbManager.getInstance();
//
//
//        try {
//            System.out.println("Fetching all...");
//
//            List<Record> allStudents = handler1.getAll("tab_student");

}
