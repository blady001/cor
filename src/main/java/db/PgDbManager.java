package db;

/**
 * Created by macblady on 21.01.2018.
 */
public class PgDbManager extends AbstractDbManager {
    private static PgDbManager instance = null;

    private PgDbManager() {}

    public static synchronized CallableDb getInstance(String url) {
        if (instance == null) {
            instance = new PgDbManager();
            System.out.println("Initialized PG db handler!");
        }
        instance.setUrl(url);
        return instance;
    }
}
