package db;

public class SqliteDbManager extends AbstractDbManager {
    private static SqliteDbManager instance = null;

    private static final String DB_URL = "jdbc:sqlite:mdb.sqlite";

    private SqliteDbManager() {

    }


    public static synchronized CallableDb getInstance(String url) {
        if (instance == null) {
            instance = new SqliteDbManager();
            System.out.println("Initialized db handler!");
        }
        instance.setUrl(url);
        return instance;
    }
}
