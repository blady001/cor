package db;

/**
 * Created by macblady on 05.12.2017.
 */
public class DbResolver {
    private DBConfig dbConfig;

    public DbResolver(DBConfig config) {
        this.dbConfig = config;
    }

    public CallableDb getDbManager() throws DbConnectorResolvingException {
        CallableDb manager;
        switch(this.dbConfig.getDbType()) {
            case "sqlite":
                manager = SqliteDbManager.getInstance(this.dbConfig.getUrl());
                break;
            case "mysql":
                manager = MySqlDbManager.getInstance(this.dbConfig.getUrl());
                break;
            case "postgresql":
                manager = PgDbManager.getInstance(this.dbConfig.getUrl());
                break;
            default:
                throw new DbConnectorResolvingException("Couldn't find suitable connector!");
        }
        return manager;
    }



    public static class DbConnectorResolvingException extends Exception {
        public DbConnectorResolvingException(String message) {
            super(message);
        }
    }

}
