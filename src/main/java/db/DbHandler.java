package db;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by macblady on 05.12.2017.
 */
public class DbHandler implements CallableDb {

    private DBConfig dbConfig;
    private CallableDb dbConnector;

    public DbHandler(DBConfig config) throws DbResolver.DbConnectorResolvingException {
        this.dbConfig = config;
        DbResolver resolver = new DbResolver(config);
        this.dbConnector = resolver.getDbManager();
    }

    public Record getOne(String tableName, String whereClause) throws SQLException {
        return this.dbConnector.getOne(tableName, whereClause);
    }

    public List<Record> getAll(String tableName) throws SQLException {
        return this.dbConnector.getAll(tableName);
    }

    public void insert(String sql) throws SQLException {
        this.dbConnector.insert(sql);
    }

    public void delete(String sql) throws SQLException {
        this.dbConnector.delete(sql);
    }

    public void update(String sql) throws SQLException {
        this.dbConnector.update(sql);
    }
}
