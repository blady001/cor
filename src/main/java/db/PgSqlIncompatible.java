package db;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by macblady on 20.11.2017.
 */
public interface PgSqlIncompatible {
    Record getOneFromPg(String tableName, String whereClause) throws SQLException;
    List<Record> getAllFromPg(String tableName) throws SQLException;
    void insertToPg(String sql) throws SQLException;
    void deleteFromPg(String sql) throws SQLException;
    void updateInPg(String sql) throws SQLException;
}
