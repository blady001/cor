package db;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by macblady on 13.11.2017.
 */
public interface CallableDb {
    Record getOne(String tableName, String whereClause) throws SQLException;
    List<Record> getAll(String tableName) throws SQLException;
    void insert(String sql) throws SQLException;
    void delete(String sql) throws SQLException;
    void update(String sql) throws SQLException;
}
